
local CONFIG = {
	duration = 10*60,
	border_radius = 100,
	spawn_radius = 80,
	render_distance = 30,
	damage_interval = 0.2,
	info_interval = 1,
	damage_amount = 1,
}

local elapsed = 0

local modname = minetest.get_current_modname()

local barrier = modname .. ":barrier"
local barrier_image = modname .. "_barrier.png"

minetest.register_node(barrier, {
	description = "Barrier",
	_doc_items_longdesc = "blub",
    drawtype = "glasslike_framed_optional",
	is_ground_content = false,
    
    walkable = false,
	diggable = false,
	pointable = false,
    
	sunlight_propagates = true,
	light_source = 10,
	use_texture_alpha = true,
	tiles = {
		{
			image = barrier_image,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.5,
			}
		},
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drop = "",
	groups = { unbreakable = 1, immortal = 1, immovable = 2 },
	on_blast = function() end,
	can_dig = function() return false end,
	on_destruct = function() end,

})

minetest.register_privilege("uhc_admin", {
	description = "controller of the uhc game",
	give_to_singleplayer = true
})

minetest.register_chatcommand("uhc_info", {
	description = "show information",
	func = function(name, param)
		minetest.chat_send_player(name, "current configuration: ")
		for i,v in pairs(CONFIG) do
			print(i,v)
			print( "- " .. i .. ": " .. v)
			minetest.chat_send_player(name, "- " .. i .. ": " .. v)
		end	
	end
})

minetest.register_chatcommand("uhc_start", {
	description = "start the game",
	privs = {uhc_admin = true},
	func = function(name, param)
		minetest.chat_send_all("---- UHC HAS STARTED! ----")
		if param ~= "ns" then spawn_players() end
		start_updating()
	end
})

minetest.register_chatcommand("uhc_adjust", {
	params = "<setting> <value>",
	description = "adjusts settings",
	privs = {uhc_admin = true},
	func = function(name, param)
		local key, value = string.match(param, "(%w+) (%d*)")
		if not key or not value then return false, "something is wrong :)" end
		minetest.chat_send_player(name, "changed " .. key .. " from " .. (CONFIG[key] or "(none)") .. " to " .. value)
		CONFIG[key] = tonumber(value)
	end
})

function chebychev_length(p) return math.max(math.abs(p.x), math.max(math.abs(p.y), math.abs(p.z))) end
function floor(p) return { x = math.floor(p.x), y = math.floor(p.y), z = math.floor(p.z) } end
function undo(f) return function(v) return f(f(f(f(f(v))))) end end

function axis_x(p) return {x= p.x,y=p.y,z=p.z} end
function axis_y(p) return {x= p.y,y=p.z,z=p.x} end
function axis_z(p) return {x= p.z,y=p.x,z=p.y} end
function axis_nx(p) return {x=-p.x,y=p.y,z=p.z} end
function axis_ny(p) return {x=-p.y,y=p.z,z=p.x} end
function axis_nz(p) return {x=-p.z,y=p.x,z=p.y} end

local placed_borders = {}
function do_axis(target, axis, posn, sizen)
	posn = axis(posn)
	local rd = CONFIG.render_distance
	if posn.x >= sizen - rd then
		for y = math.max(posn.y-rd, -sizen), math.min(posn.y+rd, sizen) do
			for z = math.max(posn.z-rd, -sizen), math.min(posn.z+rd, sizen) do
				local p = undo(axis)({x=sizen, y=y, z=z})
				local name = minetest.get_node(p).name
				if name ~= barrier and not minetest.registered_nodes[name].walkable then
					table.insert(target, p)
					table.insert(placed_borders, p)
				end
			end
		end
	end
end

function clear_previous()
	minetest.bulk_set_node(placed_borders, {name = "air"})
	if #placed_borders ~= 0 then print(string.format("cleared %d", #placed_borders)) end
	placed_borders = {}
end

function spawn_players()
	local i = 0
	local players = minetest.get_connected_players()
	for _, player in pairs(players) do
		local ang = i / #players * 3.14 * 2
		local p = {
			x = math.sin(ang) * CONFIG.spawn_radius,
			y = math.cos(ang) * CONFIG.spawn_radius,
			z = 100
		}
		player:set_pos(p)
		minetest.set_node({ x = math.floor(p.x), y = math.floor(p.y), z = math.floor(p.z) }, {name = "mcl_core:water_source"})
		i = i + 1
	end
end


local last_sizen
local last_damage_t = 0
local last_info_t = 0

function start_updating()
	minetest.register_globalstep(function(dtime)
		elapsed = elapsed + dtime
		local players = minetest.get_connected_players()
		local size = CONFIG.border_radius * (1 - elapsed / CONFIG.duration)
        local sizen = math.floor(size)

		if sizen ~= last_sizen then
			clear_previous()
			last_sizen = sizen
		end

		if last_info_t + CONFIG.info_interval <= elapsed then
			last_info_t = elapsed
			for _, player in pairs(players) do
				local distance = size - chebychev_length(player:get_pos())
				local text = string.format("%d players alive | border radius is %.2f (%.2f away)", #players, size, distance)
				local color = distance < 20 and "red" or "white"
				mcl_title.set(player, "actionbar", {text=text, color=color, stay=CONFIG.info_interval*60})
			end
		end

		if last_damage_t + CONFIG.damage_interval <= elapsed then
			last_damage_t = elapsed
			for i, player in pairs(players) do
				local p = player:get_pos()
				if math.abs(p.x) > size or math.abs(p.y) > size or math.abs(p.z) > size then
					player:set_hp(player:get_hp() - CONFIG.damage_amount)
				end
			end
		end

		for _, player in pairs(minetest.get_connected_players()) do
            local pos = player:get_pos()
			local posn = pos:floor()

			local positions = {}
			do_axis(positions, axis_x, posn, sizen)
			do_axis(positions, axis_y, posn, sizen)
			do_axis(positions, axis_z, posn, sizen)
			do_axis(positions, axis_nx, posn, sizen)
			do_axis(positions, axis_ny, posn, sizen)
			do_axis(positions, axis_nz, posn, sizen)
			minetest.bulk_set_node(positions, {name = barrier})
		end
    end)
end
