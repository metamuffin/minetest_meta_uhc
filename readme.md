# meta UHC

A minetest mod for "ultra hardcore" games.

## Features

- decrease border size
- keep track of border nodes for removal later
- only update near the player
- broadcast size in actionbar
- command for starting the game
- teleport players away from spawn
